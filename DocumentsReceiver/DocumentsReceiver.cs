﻿using System;
using System.Timers;
using System.IO;
using System.Collections.Generic;

namespace DocumentsReceiver
{
    public class DocumentsReceiver : IDisposable
    {
        public event Action DocumentsReady;
        public event Action TimedOut;

        private List<string> _files;
        private Timer _timer = new Timer();
        private FileSystemWatcher _watcher = new FileSystemWatcher();
        private bool _isWork;
        private string _path;


        public DocumentsReceiver(List<string> files)
        {
            _files = files;
            _isWork = false;
        }
        

        public void Start(string targetDirectory, int waitingInterval_ms)
        {
            _timer.Stop();
            _path = targetDirectory;  
            _watcher.EnableRaisingEvents = true;
            _timer.Interval = waitingInterval_ms;                        
            if (!_isWork)
            {
                _watcher.Changed += PathChangedHendler;
                _timer.Elapsed += TimerCallbackHendler;
                
            }
            _isWork = true;
            _timer.Start();

        }


        private void StopReceiver()
        {
            if (_isWork)
            {
                _isWork = false;
                _watcher.EnableRaisingEvents = false;
                _watcher.Changed -= PathChangedHendler;
                _timer.Stop();
                _timer.Elapsed -= TimerCallbackHendler;
            }
        }


        private void TimerCallbackHendler(object sender, ElapsedEventArgs e)
        {
            if (_isWork)
            {
                StopReceiver();
                TimedOut();
            }
        }


        private void PathChangedHendler(object sender, FileSystemEventArgs e)
        {
            foreach(var f in _files)
            {
                if(!File.Exists(_path + f))
                {
                    return;
                }
            }            

            if (_isWork)
            {
                StopReceiver();
                DocumentsReady();
            }
        }


        public void Dispose()
        {
            _watcher.Changed -= PathChangedHendler;
            _timer.Elapsed -= TimerCallbackHendler;
            _timer.Dispose();
            _watcher.Dispose();
        }
    }
}
